import os

from flask import Flask


def create_app():
    app = Flask(__name__)

    from . import dashboard
    app.register_blueprint(dashboard.dashboard_blueprint)

    from . import api
    app.register_blueprint(api.api_blueprint)


    return app
