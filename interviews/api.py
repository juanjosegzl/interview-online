import sqlite3
import subprocess
import uuid

from flask import (
    Blueprint, render_template, request, jsonify, abort
)

api_blueprint = Blueprint('api', __name__, url_prefix='/api/')


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

@api_blueprint.route('/create')
def create_instance():
    id_ = str(uuid.uuid1())
    subprocess.Popen(['sh', 'scripts/start.sh', id_])

    con = sqlite3.connect('interviews.db')
    cur = con.cursor()

    cur.execute("INSERT INTO instances VALUES (?, '', '', 'starting');", (id_,))
    con.commit()

    return jsonify({'started': 'success', 'jobId': id_})

@api_blueprint.route('/monitor/<jobid>')
def monitor(jobid):
    con = sqlite3.connect('interviews.db')
    con.row_factory = dict_factory
    cur = con.cursor()

    cur.execute("SELECT * FROM instances WHERE job_id=?", (jobid,))
    return jsonify(cur.fetchone())
        

@api_blueprint.route('/')
def get_instances():
    con = sqlite3.connect('interviews.db')
    con.row_factory = dict_factory
    cur = con.cursor()

    cur.execute("SELECT * FROM instances")
    return jsonify(cur.fetchall())
