import functools

from flask import (
    Blueprint, render_template, request, jsonify
)

dashboard_blueprint = Blueprint('dashboard', __name__, url_prefix='/')


@dashboard_blueprint.route('/')
def all_data():
    return render_template('page/index.html')
