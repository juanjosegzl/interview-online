set -x

sg_id=$(\
	aws ec2 create-security-group\
	    --group-name interview-pod\
	    --description "Security group for interview pods running ide" \
	    | jq -r ".GroupId"
     )

echo $sg_id

aws ec2 authorize-security-group-ingress --group-id $sg_id --protocol tcp --port 3000 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-id $sg_id --protocol tcp --port 5000 --cidr 0.0.0.0/0

# aws ec2 delete-security-group --group-id $sg_id
