set -x

aws ec2 create-key-pair --key-name interview --query 'InterViewKey' --output text > interview.pem

chmod 400 interview.pem
