set -x

# Frontend gives a jobid to keep track
job_id=$1

# Get security group id
sg_id=$(sh scripts/get_sg_id.sh)

instance_id=$(aws ec2 run-instances\
    --image-id ami-042e8287309f5df03\
    --count 1\
    --instance-type t2.micro\
    --security-group-ids $sg_id\
    --key-name interview | jq -r ".Instances[].InstanceId"
	   )

# Give AWS some time to assign public dns
sleep 10

dns=$(aws ec2 describe-instances --instance-ids $instance_id | jq -r ".Reservations[].Instances[].PublicDnsName")

# Update progress
sqlite3 interviews.db "UPDATE instances SET dns = '${dns}' WHERE job_id='${job_id}';"
sqlite3 interviews.db "UPDATE instances SET id = '${instance_id}' WHERE job_id='${job_id}';"
sqlite3 interviews.db "UPDATE instances SET status = 'creating' WHERE job_id='${job_id}';"

sleep 60

get_status () {
    aws ec2 describe-instance-status --instance-ids $instance_id | jq -r ".InstanceStatuses[].InstanceStatus.Status"
}

status=$(get_status)

until [ "$status" == "ok" ]
do
    sleep 60
    status=$(get_status)
done

# Ok is ready to connect using ssh

sqlite3 interviews.db "UPDATE instances SET status = 'running' WHERE id='${instance_id}';"


# Trust certificate
ssh-keyscan -t rsa ${dns} >> ~/.ssh/known_hosts

# TODO parameterize project
scp -i interview.pem projects/coffee.tar.gz ubuntu@${dns}:/home/ubuntu/project.tar.gz
scp -i interview.pem scripts/init.sh ubuntu@${dns}:

# Install docker and run theia container, detach job
ssh -i interview.pem -f ubuntu@${dns} "sh init.sh &"

sqlite3 interviews.db "UPDATE instances SET status = 'creating_ide' WHERE id='${instance_id}';"

ide_is_available () {
    curl -Is ${dns}:3000 | head -1
}

# check for theia availability
available=$(ide_is_available)
until [[ "$available" == *"200"* ]]
do
    sleep 10
    available=$(ide_is_available)
done

sqlite3 interviews.db "UPDATE instances SET status = 'ready' WHERE id='${instance_id}';"

# schedule termination
echo "bash /home/ubuntu/interview-online/scripts/terminate.sh ${instance_id}" | at now + 120 minutes
