cd ~

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker ubuntu

tar -xzf project.tar.gz 
cd project
sudo docker run --init -p 3000:3000 -p 5000:5000 -v "$(pwd):/home/project" theiaide/theia-python:next &
