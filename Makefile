run:
	FLASK_APP=interviews FLASK_DEBUG=1 flask run -h 0.0.0.0

clean:
	@find . \( -name "*~" -o -name "*pyc" \) -delete
	@rm -rf .coverage htmlcov
test:
	pytest -v

coverage:
	-coverage run -m pytest
	coverage report -m

createdb:
	sqlite3 interviews.db "CREATE TABLE instances (job_id, id, dns, status);"

.PHONY: run clean
